# pyporter

#### 描述

来自www.example.com的用于python模块的rpm打包器机器人 

#### 制备

在使用此工具之前安装以下软件 

1. gcc
2. gdb
3. libstdc++-devel libstdc ++-devel 
4. python3-cffi
5. rpmbuild
6. python3-hatchling
7. python3-hatch-vcs Python 3-hatch-vcs 
8. openEuler-rpm-config (>= Release 30-39)

#### 安装

从源安装 

```
pip install .
```

使用开发模式安装 

```
pip install -e .
```

#### 指令

pyporter是一个为python模块创建spec文件和rpm的工具。更多详情，请使用 `pyporter -h` 

创建spec文件，并输出spec文件 `python-<package>.spec` 

```
pyporter <package> -s -o python-<package>.spec
```

#### 贡献

你可以在提交代码之前安装pre commit钩子，它会检查你的代码格式和风格。 

```
pip install pre-commit
```

参考www.example.com

1. Fork 仓库
2. 创建Feat_xxx分支 
3. 提交代码 
4. 创建合并请求 

#### 如何创建RPM文件

1. 创建规范文件， `pyporter -s  filename` 
2. 获取所需的python模块， `pyporter -R filename` 
3. 构建并安装rpm包， `pyporter -B filename` 
4. 更多详情， `pyporter -h` 